from django import forms
from .models import Task

class FormTaskAdd(forms.ModelForm):
    name = forms.CharField(max_length=40)
    description = forms.CharField(widget=forms.Textarea, max_length=200)
    due_date = forms.CharField(max_length=40)

    class Meta:
    	model = Task
    	fields = ('name', 'description', 'due_date')