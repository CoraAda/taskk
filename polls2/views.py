from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect

from .models import Task
from .forms import FormTaskAdd

def index(request):
    return render(request, 'polls2/index.html')

def add_task(request):
    if request.method == 'POST':
        form = FormTaskAdd(request.POST)
        if form.is_valid():
            # task = form.save(commit=False)
            form.save()
            return render(request, 'polls2/task_list.html')
    else:        
        form = FormTaskAdd()
    return render(request, 'polls2/add_task.html', {'form':form})

def task_list(request):
    tasks = Task.objects.all()
    return render(request, 'polls2/task_list.html', {'task_list':tasks})

def del_task(request, pk):
    tasks = get_object_or_404(Task, pk = pk)
    tasks.delete()
    return HttpResponseRedirect("/tasks/")

def edit_task(request, pk):
    task = Task.objects.get(pk=pk)
    if request.POST:
        form=FormTaskAdd(request.POST,instance=task)
        if form.is_valid():
            form.save()
            return render(request, 'polls2/task_list.html')
    else:
        form=FormTaskAdd(instance=task)

    return render(request, 'polls2/task_edit.html', {'form':form, 'task': task}) 