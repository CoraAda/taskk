from django.contrib import admin

# Register your models here.
from .models import Task
from django.db import models



class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')

admin.site.register(Task, TaskAdmin)