from django.conf.urls import url

from views import index, add_task, task_list, del_task, edit_task

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add/$', add_task, name='add_task'),
    url(r'^tasks/$', task_list, name='tasks'),
    url(r'^delete/(?P<pk>[0-9]+)/$', del_task, name='delete'),
    url(r'^edit/(?P<pk>[0-9]+)/$', edit_task, name='edit_task'),
]

