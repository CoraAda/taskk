from django.db import models
import datetime
from django.utils import timezone
from django.core.urlresolvers import reverse

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    pub_date = models.DateTimeField(auto_now_add=True)
    due_date = models.DateField('due date')
    modification_date = models.DateField(auto_now=True)

    
    def __str__(self):
        return self.name

    def delete_task(self):
        return reverse('task_manager:delete', args=(self.pk,))

    @property
    def task_edit(self):
    	return reverse('task_manager:edit_task', args=(self.pk,))